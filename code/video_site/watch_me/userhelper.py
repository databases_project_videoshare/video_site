from .models import XUser
import re

email_regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'


def read_user_from_post(request):
    new_user = XUser()
    new_user.first_name = request.POST.get('first_name', '')
    new_user.last_name = request.POST.get('last_name', '')
    new_user.country_code_id = request.POST.get('country_code', '')
    new_user.password = request.POST.get('password', '')
    new_user.email = request.POST.get('email','')
    return new_user


def validate_user(new_user, request):
    errors = list()
    if new_user.first_name == '' or new_user.last_name == '':
        errors.append('Don\'t leave the name empty')
    if new_user.password == '':
        errors.append('Don\'t leave the password empty')
    if request.POST['re_password'] != new_user.password:
        errors.append('The two given passwords are not the same!')
    if new_user.country_code_id == '':
        errors.append('Please select a country from the list!')
    if not re.search(email_regex, new_user.email):
        errors.append('Please give a valid e-mail address!')
    if new_user.picture is None:
        errors.append('Invalid profile picture')
    if len(errors) > 0:
        return False
    else:
        return True


def get_user_by_id(id):
    try:
        user = XUser.objects.filter(id=id).get()
        return user
    except Exception as e:
        print(e)
        return None
