from django.contrib import admin
from .models import *

admin.site.register(XVideo)
admin.site.register(XCategory)
admin.site.register(XUser)
admin.site.register(XComments)
admin.site.register(XCountry)
admin.site.register(XplaylistVideo)
admin.site.register(XFeedback)
admin.site.register(XLike)
admin.site.register(XNotificatonType)
admin.site.register(XNotification)
admin.site.register(XReportTypes)
admin.site.register(XReport)
admin.site.register(XSearchHistory)
admin.site.register(XSubscribe)
admin.site.register(XTag)
admin.site.register(XVideoTag)
admin.site.register(XWatchHistory)













