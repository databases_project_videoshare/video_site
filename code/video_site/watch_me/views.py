import random
from itertools import chain

from django.contrib.auth.hashers import make_password, check_password
from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import render, redirect
from django.db.models import Q
from django.db import connection
from datetime import datetime

from . import queryhelper
from .forms import *
from .userhelper import read_user_from_post, validate_user, get_user_by_id
from .videohelper import video_from_post, save_history, is_liked, add_video_to_playlist, save_tags, save_search
from .subhelper import is_subbed, get_sub


def index(request):
    if request.session.get('user_id', 0) == 0:
        context = {'latest_videos': XVideo.objects.order_by('-upload_date')[:9]}
    else:
        user_id = request.session['user_id']
        context = {'latest_videos': XVideo.objects.order_by('-upload_date')[:9],
                   'notificaiton_count': len(XNotification.objects.filter(user__id=user_id).all())}
    return render(request, 'projectx/index.html', context)


def adminpanel(request):
    if request.session.get('user_id', 0) == 4206969:
        user_id = request.session['user_id']
        user = get_user_by_id(request.session['user_id'] if id == 0 else id)

        # incoming feedback
        feedbacks = XFeedback.objects.all()
        feedback_list = [{
            "date": feedback.xdate,
            "text": feedback.text,
            "rating": feedback.rating,
            "version": feedback.version,
            "user": f"{XUser.objects.filter(id=feedback.user_id).get().first_name} {XUser.objects.filter(id=feedback.user_id).get().last_name}"
        } for feedback in feedbacks]

        # Reports
        reports = XReport.objects.all()
        report_list = [{
            'date': report.r_date,
            'type': report.r_type.name,
            'video_id': report.r_video_id,
            'video_title': f"{XVideo.objects.filter(id=report.r_video_id).all()[0].name}",
            'text': report.r_text}
            for report in reports]

        # categories
        categories = [{"id": cat.id, "name": cat.name} for cat in XCategory.objects.all()]

        try:
            return render(request, 'projectx/adminpanel.html', {
                "user": user,
                "feedbacks": feedback_list,
                "reports": report_list,
                "categories": categories,
            })
        except Exception as e:
            print(f"{e=}")
            return HttpResponse('Error while getting the admin page')
    else:
        return redirect("watch_me:index")


def send_feedback(request):
    try:
        user_id = request.session.get('user_id', 0)
        if user_id > 0:
            if request.method == "POST":
                new_feedback = XFeedback()
                new_feedback.xdate = datetime.datetime.now()
                new_feedback.text = request.POST.get("feedback_textarea", "")
                new_feedback.rating = request.POST.get("rating", "")
                new_feedback.version = "0.1"
                new_feedback.user = XUser.objects.filter(id=user_id).get()
                print(f"{new_feedback=}")
                new_feedback.save()
        return redirect("watch_me:index")
    except Exception as ex:
        print(f"{ex=}")
        return redirect("watch_me:index")


def add_to_playlist(request):
    try:
        user_id = request.session.get('user_id', 0)
        if user_id > 0:
            if request.method == "POST":
                hozzaad = XplaylistVideo()
                hozzaad.playlist_id = request.POST.get("playlist_id", "")
                hozzaad.video_id = request.POST.get("video_id", "")
                hozzaad.save()
        return redirect("watch_me:watch_video", request.POST.get("video_id", ""))
    except Exception as ex:
        print(f"{ex=}")
        return redirect("watch_me:index")


def send_report(request):
    try:
        user_id = request.session.get('user_id', 0)
        if user_id > 0:
            if request.method == "POST":
                new_report = XReport()
                new_report.r_text = request.POST.get("report_textarea", "")
                new_report.r_date = datetime.datetime.now()
                new_report.r_type_id = request.POST.get("report_type", "")
                new_report.r_video_id = request.POST.get("video_id", "")
                print(f"{new_report=}")
                new_report.save()
        return redirect("watch_me:watch_video", request.POST.get("video_id", ""))
    except Exception as ex:
        print(f"{ex=}")
        return redirect("watch_me:index")


def edit_category(request, id, new_name):
    short_name = new_name[:20]
    print(f"edit category: {id=}, {short_name=}")
    if request.session.get('user_id', 0) == 4206969:
        XCategory.objects.filter(id=id).update(name=short_name)
        try:
            return redirect("watch_me:adminpanel")
        except Exception as e:
            print(f"{e=}")
            return HttpResponse('Error while getting the admin page')
    else:
        return redirect("watch_me:index")


def delete_category(request, id):
    print(f"delete category: {id=}")
    if request.session.get('user_id', 0) == 4206969:
        XCategory.objects.filter(id=id).delete()
        try:
            return redirect("watch_me:adminpanel")
        except Exception as e:
            print(f"{e=}")
            return HttpResponse('Error while getting the admin page')
    else:
        return redirect("watch_me:index")


def add_category(request, name):
    short_name = name[:20]
    print(f"add category: {short_name=}")
    if request.session.get('user_id', 0) == 4206969:
        new_category = XCategory(name=short_name)
        new_category.save()
        try:
            return redirect("watch_me:adminpanel")
        except Exception as e:
            print(f"{e=}")
            return HttpResponse('Error while getting the admin page')
    else:
        return redirect("watch_me:index")


def logout(request):
    if request.session.get('user_id', 0) != 0:
        user = XUser.objects.filter(id=request.session['user_id']).get()
        user.last_online = datetime.datetime.now()
        user.save()
        request.session['user_id'] = 0
        return redirect(f'watch_me:index')
    else:
        return HttpResponse('You are already logged out go back to the main page')


def login(request):
    if request.method == "POST":
        email, password = request.POST['email'], request.POST['password']
        user = XUser()
        try:
            user = XUser.objects.get(email=email)
        except:
            user = None

        if user is None:
            return HttpResponse('There is no user with this email')

        if check_password(password, user.password):
            request.session['user_id'] = user.id
            request.session['user_name'] = user.first_name + ' ' + user.last_name
            print(request.session['user_id'])

        return redirect(f'watch_me:index')
    else:
        return render(request, 'projectx/login.html', {})


def register(request):
    if request.method == "POST":
        new_user = read_user_from_post(request)
        if validate_user(new_user, request):
            print(f'New user added to the database! {new_user.first_name} {new_user.last_name}')
            new_user.password = make_password(new_user.password)
            new_user.reg_date = datetime.datetime.now()
            new_user.save()
            new_user.picture = request.FILES['p_pic']
            new_user.save()
        else:
            print("There is an error, while adding the new user!")
        return redirect(f'watch_me:login')
    else:
        form = XUserForm()
        return render(request, 'projectx/register.html', {'form': form})


def profile(request, id=0):
    if id == 0 and request.session.get('user_id', 0) == 0:
        return HttpResponse("You have to be logged in, if you want to look your profile")
    user = get_user_by_id(request.session['user_id'] if id == 0 else id)
    sub_count = len(XSubscribe.objects.filter(to_id=request.session['user_id'] if id == 0 else id))
    video_count = XVideo.objects.filter(uploader_id=user.id).count()
    if user is not None:
        if request.session.get('user_id', 0) == 0:
            request.session['user_id'] = 0
        return render(request, 'projectx/profile.html', {'user': user,
                                                         'is_subbed': is_subbed(request.session['user_id'], user.id),
                                                         "video_count": video_count,
                                                         'sub_count': sub_count})
    else:
        return redirect('watch_me:index')


def upload_video(request):
    if request.session.get('user_id', 0) == 0:
        return HttpResponse('You can upload video, while you are logged in')

    if request.method == 'POST' and request.FILES['v_video']:
        video = video_from_post(request)
        video.save()
        save_tags(video.id, request.POST.get('v_tags', ''))
        add_video_to_playlist(video)
        return redirect('watch_me:index')
    else:
        video_form = XVideoForm()
        return render(request, 'projectx/upload_video.html', {'video_form': video_form})


def watch_video(request, id):
    try:
        video = XVideo.objects.filter(id=id).get()
        if XVideo.objects.filter(uploader__id=video.uploader_id).count() > 0:
            uploader_other_videos = XVideo.objects.filter(uploader__id=video.uploader_id).exclude(id=video.id).all()
        tags = ["'" + tag.id + "'" for tag in XTag.objects.raw(queryhelper.SELECT_TAGS_FOR_VIDEO(video.id))]
        similar_args = ((','.join(tags)))
        similar_videos = [video for video in
                          XVideo.objects.raw(queryhelper.SELECT_SIMILAR_VIDEOS(video).format(similar_args))]

        if video.comments_on == 1:
            comments = [comm for comm in XComments.objects.raw(queryhelper.SELECT_COMMENTS_WITH_USER(id))]
        else:
            comments = None
    except Exception as e:
        video = None
        comments = None
        print(e)

    if video is None:
        return HttpResponse(f'There is no video, with the given ID ({id})')

    try:
        like_count = XLike.objects.filter(video__id=video.id).count()
        liked = is_liked(request.session.get('user_id', 0), id)
    except Exception as e:
        print(e)
        like_count = "unknown"

    try:
        tags_raw = XVideoTag.objects.filter(video=video.id).all()
        tags = [XTag.objects.filter(id=tag_raw.tag.id).all()[0].name for tag_raw in tags_raw]
    except Exception as e:
        print(e)
        tags = []

    try:
        report_types = [{"id": type.id, "name": type.name} for type in XReportTypes.objects.all()]
    except Exception as e:
        print(f"{e=}")
        report_types = []

    # lehet innen ki kell majd venni az auto generated playlisteket, mert ugye azokba nem birunk hozzaadni
    playlists = [{"id": playlist.id, "name": playlist.name} for playlist in XPlaylist.objects.all()]

    save_history(request, id)
    if comments is None:
        return render(request, 'projectx/video.html', {'video': video,
                                                       "video_id": id,
                                                       'like_count': like_count,
                                                       'tags': tags,
                                                       'liked': liked,
                                                       'similar_videos': similar_videos,
                                                       'other_videos': uploader_other_videos,
                                                       "report_types": report_types,
                                                       "playlists": playlists})
    else:
        return render(request, 'projectx/video.html', {'video': video,
                                                       "video_id": id,
                                                       'comments': comments,
                                                       'like_count': like_count,
                                                       'tags': tags,
                                                       'liked': liked,
                                                       'similar_videos': similar_videos,
                                                       'other_videos': uploader_other_videos,
                                                       "report_types": report_types,
                                                       "playlists": playlists})


def comment(request, id):
    if request.session.get('user_id', 0) == 0:
        return HttpResponse('You can comment a video, while you are logged in')

    comment = XComments()
    comment.c_text = request.POST.get('comment', '')
    if comment.c_text == '':
        return HttpResponse('You can not submit an empty comment')
    comment.user_id = request.session['user_id']
    comment.video_id = id
    comment.c_date = datetime.datetime.now()
    comment.save()
    return redirect('watch_me:watch_video', id=id)


def playlist(request, id):
    try:
        list = XPlaylist.objects.filter(id=id).get()
        videos = XVideo.objects.filter(xplaylistvideo__playlist_id=id)
        return render(request, 'projectx/playlist.html', {'playlist': list,
                                                          'videos': videos})
    except Exception as e:
        print(e)
        return HttpResponse('An error occured, while tried to look the playlist')


def subscribe(request, id):
    if request.session.get('user_id', 0) == 0:
        return HttpResponse('You can subscibe to another user, while you are logged in')

    user_id = request.session['user_id']
    sub = XSubscribe()
    try:
        sub.from_u_id = user_id
        sub.to_id = id
        sub.save()
        return redirect('watch_me:profile', id=id)
    except Exception as e:
        print(e)
        return HttpResponse('An error occured, while tried to sub')


def unsubscribe(request, id):
    if request.session.get('user_id', 0) == 0:
        return HttpResponse('You can unsubscibe from another user, while you are logged in')
    user_id = request.session['user_id']
    sub = get_sub(user_id, id)
    try:
        sub.delete()
        return redirect('watch_me:profile', id=id)
    except Exception as e:
        print(e)
        return HttpResponse('An error occured, while tried to unsub')


def notifications(request, id=0):
    if request.session.get('user_id', 0) == 0:
        return HttpResponse('Log it to see your notifications')

    user_id = request.session['user_id']
    user = get_user_by_id(request.session['user_id'] if id == 0 else id)
    notis = XNotification.objects.filter(user__id=user.id).all()
    extended_notis = [{'video_id': f"{noti.video_id}",
                       'id': f"{noti.id}",
                       'video_title': f"{XVideo.objects.filter(id=noti.video_id).all()[0].name}",
                       'type': str(noti.n_type),
                       'send_date': str(noti.sent_date)} for noti in notis]

    try:
        return render(request, 'projectx/notifications.html', {'user': user, 'notis': extended_notis})
    except Exception as e:
        print(f"{e=}")
        return HttpResponse('An error occured, while getting your notifications')


def delete_notification(request, id, video_id=0):
    try:
        if video_id == 0:
            XNotification.objects.filter(id=id).delete()
            return redirect('watch_me:notifications')
        else:
            XNotification.objects.filter(id=id).delete()
            return redirect('watch_me:watch_video', id=video_id)
    except Exception as ex:
        print(f"{ex=}")
        return redirect('watch_me:index')


def my_videos(request):
    if request.session.get('user_id', 0) == 0:
        return HttpResponse('Log it to see your videos')
    user = get_user_by_id(request.session['user_id'])
    videos = XVideo.objects.filter(uploader__id=user.id).all()
    try:
        return render(request, 'projectx/my_videos.html', {'user': user, 'videos': videos})
    except Exception as e:
        print(e)
        return HttpResponse('An error occured, while getting your notifications')

def videos_user(request, userid):
    try:
        videos = XVideo.objects.filter(uploader__id=userid).all()
        return render(request, 'projectx/videos_user.html', {'videos': videos})
    except Exception as e:
        print(e)
        return HttpResponse('An error occured, while getting the videos')


def delete_profile(request, id):
    try:
        user = XUser.objects.filter(id=id).get()
        user.delete()
        print('The user deleted succesfully')
        if request.session.get('user_id', 0) != 0:
            request.session['user_id'] = 0
    except Exception as ex:
        print(ex)
    finally:
        return redirect('watch_me:index')


def subscribers(request, id):
    subs = XSubscribe.objects.filter(to_id=id).all()
    subs = XUser.objects.all().filter(id__in=[id.from_u_id for id in subs])
    user = get_user_by_id(id)
    print(f'{len(subs)}')
    return render(request, 'projectx/subsribers.html',
                  {'user': user, 'subs': subs})


def search(request):
    keyword = request.GET.get('query', '')
    keyword_with_like = '%' + keyword + '%'
    try:
        if request.session.get('user_id', 0) != 0:
            save_search(keyword, request.session['user_id'])
        video_args = (keyword_with_like, keyword_with_like, keyword_with_like, keyword_with_like)
        videos = [video for video in XVideo.objects.raw(queryhelper.SEARCH_FOR_VIDEOS(keyword), video_args)]

        user_args = (keyword_with_like, keyword_with_like, keyword_with_like)
        users = [user for user in XUser.objects.raw(queryhelper.SEARCH_FOR_USERS(keyword), user_args)]

        p_lists_args = video_args
        p_lists = [p_list for p_list in XPlaylist.objects.raw(queryhelper.SEARCH_FOR_PLAYLIST(keyword), p_lists_args)]
        result = list(chain(videos, users, p_lists))
        random.shuffle(result)
        return render(request, 'projectx/result.html', {'result': result})
    except Exception as e:
        print(e)
        return HttpResponseNotFound()


def liked_videos(request):
    if request.session.get('user_id', 0) == 0:
        return HttpResponse('Log it to see your liked videos')

    videos = XVideo.objects.filter(xlike__user_id=request.session['user_id'])
    return render(request, 'projectx/liked_videos.html', {'videos': videos})


def like(request, id):
    if request.session.get('user_id', 0) == 0:
        return HttpResponse('Log it to like the video')
    try:
        like = XLike()
        like.video_id = id
        like.user_id = request.session['user_id']
        like.save()
    except Exception as ex:
        print(ex.with_traceback)
    finally:
        return redirect('watch_me:watch_video', id=id)


def dislike(request, id):
    if request.session.get('user_id', 0) == 0:
        return HttpResponse('Log it to dislike the video')
    try:
        XLike.objects.filter(video__id=id, user__id=request.session['user_id']).delete()
    except Exception as ex:
        print(ex.with_traceback)
    finally:
        return redirect('watch_me:watch_video', id=id)


def most_viewed(request, cat=''):
    categories = [cat.name for cat in XCategory.objects.all()]
    if cat not in categories and cat != '':
        return HttpResponse('Bad category, try again')
    ids = [v.video_id for v in XWatchHistory.objects.raw(queryhelper.SELECT_MOST_VIEWED_VIDEOS())]
    try:
        if cat != '':
            print(ids)
            print(cat)
            print(cat.capitalize())
            videos = XVideo.objects.filter(id__in=ids, category__name=cat.capitalize()).all()
        else:
            videos = XVideo.objects.filter(id__in=ids).all()

        print(videos)
    except Exception as ex:
        print(ex)
        return HttpResponse('There was an error, look the report in the terminal')
    return render(request, 'projectx/most_viewed.html', {'videos': videos,
                                                         'categories': categories})


def most_active_users(request, filter=''):
    if filter not in ('comment, upload') and filter != '':
        return HttpResponse('Bad filter, try again')
    merged = list()
    for user in XUser.objects.all():
        with connection.cursor() as cursor:
            comment_c = cursor.var(int).var
            video_c = cursor.var(int).var
            cursor.callproc('GET_USER_ACTIVITY_UPLOAD', [user.id, video_c])
            cursor.callproc('GET_USER_ACTIVITY_COMMENT', [user.id, comment_c])

            class stat: pass

            stat = stat()
            stat.id = user.id
            stat.name = user.first_name + ' ' + user.last_name
            stat.comment_c = comment_c.getvalue()
            stat.video_c = video_c.getvalue()
            stat.combined = stat.comment_c + stat.video_c
            merged.append(stat)

    if filter == '':
        merged.sort(key=lambda x: x.combined, reverse=True)
    elif filter == 'comment':
        merged.sort(key=lambda x: x.comment_c, reverse=True)
    else:
        merged.sort(key=lambda x: x.video_c, reverse=True)

    return render(request, 'projectx/most_active_users.html', {'merged': merged})


def playlists(request):
    try:
        play_lists = [ps for ps in XPlaylist.objects.raw(queryhelper.SELECT_PLAYLISTS_ORDERED_BY_VIEW())]
        return render(request, 'projectx/playlists.html', {'play_lists': play_lists})
    except Exception as ex:
        print(ex)
        return redirect('watch_me:index')


def favorites(request):
    if request.session.get('user_id', 0) == 0:
        return HttpResponse('You can watch your favorite videos, when you are logged in')
    try:
        videos = XVideo.objects.filter(xlike__user_id=request.session['user_id']).all()
        return render(request, 'projectx/favorite.html', {'videos': videos})
    except Exception as ex:
        print(ex)
        return redirect('watch_me:index')


def delete_comment(request, video, id):
    try:
        XComments.objects.filter(id=id).delete()
        return redirect('watch_me:watch_video', id=video)
    except Exception as ex:
        print(ex)
        return redirect('watch_me:index')


def others_who_likes_video(request, video_id):
    try:
        users = [user for user in XUser.objects.raw(queryhelper.SELECT_USERS_WHO_LIKES_THE_VIDEO(video_id))]
        return render(request, 'projectx/users_who_likes.html', {'users': users})
    except Exception as ex:
        print(ex)
        return redirect('watch_me:index')


def delete_video(request, id):
    try:
        XVideo.objects.filter(id=id).delete()
        return redirect('watch_me:my_videos')
    except Exception as ex:
        print(ex)
        redirect('watch_me:my_videos')


def stats_for_video(request, id):
    class Stat: pass
    try:
        video = XVideo.objects.filter(id=id).get()
        stat = Stat()
        stat.number_of_views = [res for res in XWatchHistory.objects.raw(queryhelper.SELECT_VIEWS_OF_VIDEO(video.id))][0].id
        # views_by_time(id)
        return render(request, 'projectx/stat.html', {'video': video,
                                                      'stat': stat})
    except Exception as ex:
        print(ex)
        return redirect('watch_me:watch_video', id=id)


def delete_playlist(request, id):
    try:
        XPlaylist.objects.filter(id=id).delete()
        return redirect('watch_me:playlists')
    except Exception as ex:
        print(ex)
        return redirect('watch_me:playlist', id = id)


def add_playlist(request, name):
    if request.session.get('user_id', 0) == 0:
        return HttpResponse('Log in to create a new playlist')

    try:
        p_list = XPlaylist()
        p_list.name = name[:20]
        p_list.creator_user_id = request.session['user_id']
        p_list.c_date = datetime.date.today()
        p_list.save()
    except Exception as ex:
        print(ex)
    finally:
        return redirect('watch_me:playlists')