from django import forms
from .models import *


class XUserForm(forms.ModelForm):
    class Meta:
        model = XUser
        fields = {'id', 'first_name', 'last_name', 'password', 'email', 'country_code'}


class XVideoForm(forms.ModelForm):
    class Meta:
        model = XVideo
        fields = {'name', 'description', 'comments_on', 'category', 'id'}
