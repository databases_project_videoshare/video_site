def SELECT_COMMENTS_WITH_USER(video_id):
    return f'''SELECT u.first_name || ' ' || u.last_name as u_name,
                      c.USER_ID as user_id,
                      c.ID as id,
                      c.C_TEXT as text,
                      c.C_DATE as c_date
               FROM xcomments c
               INNER JOIN xuser u on u.id = c.user_id
               WHERE c.VIDEO_ID = {video_id}
               ORDER BY c.C_DATE DESC;'''


def SELECT_MOST_VIEWED_VIDEOS():
    return '''SELECT 1 as id, video_id, COUNT(user_id) 
            FROM xwatch_history 
            GROUP BY video_id 
            ORDER BY COUNT(user_id) DESC;'''


def SELECT_PLAYLIST_ID_FOR_CATEGORY(category_id):
    return f'''SELECT p.id from xplaylist p, xcategory c
               where p.name = c.name and c.id = {category_id};'''


def SELECT_USERS_WHO_LIKES_THE_VIDEO(video_id):
    return f'''select DISTINCT u.FIRST_NAME || ' ' || u.LAST_NAME as user_name,
                               u.id as id
               from xuser u
               full outer join xlike l on l.user_id = u.id
               full outer join xplaylist p on p.creator_user_id = u.id
               full outer join xplaylist_video pv on pv.playlist_id = p.id 
               where l.video_id = {video_id} or pv.video_id = {video_id}
               order by u.id'''


def SEARCH_FOR_VIDEOS(keyword):
    return f''' SELECT DISTINCT V.*, 'VIDEO' AS T
                FROM XVIDEO V
                LEFT JOIN XUSER U ON U.ID = V.UPLOADER_ID
                LEFT JOIN XVIDEO_TAG XT ON V.ID = XT.VIDEO_ID
                LEFT JOIN XTAG T ON XT.TAG_ID = T.ID
                WHERE lower(V.NAME) LIKE lower(%s) OR 
                      lower(U.LAST_NAME) LIKE lower(%s) OR 
                      lower(U.FIRST_NAME) LIKE lower(%s) OR 
                      lower(T.NAME) LIKE lower(%s)
                ORDER BY V.UPLOAD_DATE DESC
               '''


def SEARCH_FOR_USERS(keyword):
    return f'''SELECT DISTINCT U.*, 'USER' AS T
               FROM XUSER U
               INNER JOIN XVIDEO V ON V.UPLOADER_ID = U.ID
               WHERE lower(U.FIRST_NAME) LIKE lower(%s) OR 
                     lower(U.LAST_NAME) LIKE lower(%s)OR
                     lower(V.NAME) LIKE lower(%s)
               ORDER BY U.ID
               '''


def SEARCH_FOR_PLAYLIST(keyword):
    return f'''SELECT DISTINCT P.*, 'PLAYLIST' AS T
               FROM XPLAYLIST P
               LEFT JOIN XPLAYLIST_VIDEO PV on P.ID = PV.PLAYLIST_ID
               LEFT JOIN XUSER U ON p.creator_user_id = U.ID
               FULL OUTER JOIN XVIDEO V ON V.ID = PV.VIDEO_ID
               WHERE lower(P.NAME) LIKE lower(%s) OR 
                     lower(V.NAME) LIKE lower(%s) OR
                     lower(U.FIRST_NAME) LIKE lower(%s) OR
                     lower(U.LAST_NAME) LIKE lower(%s) 
               ORDER BY P.C_DATE DESC
               '''


def SELECT_SIMILAR_VIDEOS(video):
    return '''SELECT DISTINCT V.*
               FROM XVIDEO V
               INNER JOIN XVIDEO_TAG VT ON VT.VIDEO_ID = V.ID
               INNER JOIN XTAG T ON T.ID = VT.TAG_ID
               INNER JOIN XCATEGORY C ON C.ID = V.CATEGORY_ID
               WHERE (lower(T.NAME) IN ({}) OR  
                     C.ID = ''' + str(video.category_id) + ''') AND
                     V.ID != ''' + str(video.id) + '''
               ORDER BY V.ID'''


def SELECT_TAGS_FOR_VIDEO(id):
    return f'''SELECT T.NAME as ID
               FROM XTAG T
               INNER JOIN XVIDEO_TAG VT ON VT.TAG_ID = T.ID
               WHERE VT.VIDEO_ID = {id}
               ORDER BY T.NAME
    '''


def SELECT_VIEWS_OF_VIDEO(id):
    return f'''SELECT COUNT(H.USER_ID) AS ID
               FROM XWATCH_HISTORY H
               WHERE H.VIDEO_ID = {id}
               GROUP BY H.VIDEO_ID'''

def SELECT_VIDEO_VIEWS_FOR_DAYS(id):
    return f'''select to_char(w_date, 'yyyy-mm-dd') as datas, count(*)
               from xwatch_history
               where video_id = {id}
               group by to_char(w_date, 'yyyy-mm-dd')'''


def SELECT_PLAYLISTS_ORDERED_BY_VIEW():
    return f'''SELECT ps.id as id, 
                      ps.name as name,
                      SUM((select count(wh.id) view_count
                           from xvideo v
                           inner join xwatch_history wh on wh.video_id = v.id
                           where pv.video_id = v.id
                           group by v.id, v.name)) as net_view
               FROM xplaylist ps
               INNER JOIN xplaylist_video pv on pv.playlist_id = ps.id
               GROUP BY ps.id, ps.name, 3
               ORDER BY 3 DESC'''

