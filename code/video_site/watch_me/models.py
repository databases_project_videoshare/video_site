import datetime

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from .pathhelper import set_file_path, profile_path


class XCategory(models.Model):
    id = models.AutoField(primary_key=True, auto_created=True)
    name = models.CharField(max_length=20, null=False)

    class Meta:
        db_table = 'xcategory'

    def __str__(self):
        return self.name


class XCountry(models.Model):
    code = models.CharField(max_length=4, primary_key=True, null=True)
    name = models.CharField(max_length=100, null=False)

    class Meta:
        db_table = 'xcountry'

    def __str__(self):
        return f'{self.name}  '


class XUser(models.Model):
    id = models.AutoField(primary_key=True, null=False, auto_created=True)
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    password = models.CharField(max_length=256)
    email = models.CharField(max_length=254)
    reg_date = models.DateTimeField(default=datetime.date.today, null=False, )
    country_code = models.ForeignKey(XCountry, on_delete=models.CASCADE)
    last_online = models.DateTimeField(null=True)
    picture = models.FileField(upload_to=profile_path, null=False)

    class Meta:
        db_table = 'xuser'

    def __str__(self):
        return self.first_name + ' ' + self.last_name


class XVideo(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, null=False)
    description = models.CharField(max_length=255, blank=True, null=True)
    binary = models.FileField(upload_to=set_file_path, null=False)
    thumbnail = models.FileField(upload_to=set_file_path, null=False)
    length = models.IntegerField(null=False)
    comments_on = models.IntegerField(null=False, validators=[MinValueValidator(0), MaxValueValidator(1)])
    category = models.ForeignKey(XCategory, on_delete=models.CASCADE)
    uploader = models.ForeignKey(XUser, on_delete=models.CASCADE)
    upload_date = models.DateTimeField(default=datetime.date.today, null=False)

    class Meta:
        db_table = 'xvideo'

    def __str__(self):
        return self.name


class XComments(models.Model):
    user = models.ForeignKey(XUser, on_delete=models.CASCADE)
    video = models.ForeignKey(XVideo, on_delete=models.CASCADE)
    c_date = models.DateTimeField(blank=True, null=True)
    c_text = models.CharField(max_length=254, blank=True, null=True)

    class Meta:
        db_table = 'xcomments'


class XFeedback(models.Model):
    xdate = models.DateField(blank=True, null=True)
    user = models.ForeignKey(XUser, on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    rating = models.IntegerField(default=3, null=False, validators=[MinValueValidator(1), MaxValueValidator(5)])
    version = models.CharField(default="v1.0", max_length=5)

    class Meta:
        db_table = 'xfeedback'


class XLike(models.Model):
    user = models.ForeignKey(XUser, on_delete=models.CASCADE)
    video = models.ForeignKey(XVideo, on_delete=models.CASCADE)
    l_date = models.DateField(default=datetime.date.today)

    class Meta:
        db_table = 'xlike'


class XNotificatonType(models.Model):
    id = models.AutoField(primary_key=True)
    text = models.CharField(max_length=254, blank=True, null=True)

    class Meta:
        db_table = 'xnotificaton_type'

    def __str__(self):
        return self.text


class XNotification(models.Model):
    id = models.AutoField(primary_key=True)
    video = models.ForeignKey(XVideo, on_delete=models.CASCADE)
    user = models.ForeignKey(XUser, on_delete=models.CASCADE)
    n_type = models.ForeignKey(XNotificatonType, on_delete=models.CASCADE)
    sent_date = models.DateField(default=datetime.date.today)

    class Meta:
        db_table = 'xnotification'


class XPlaylist(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, blank=True, null=True)
    c_date = models.DateField(default=datetime.date.today)
    creator_user = models.ForeignKey(XUser, on_delete=models.CASCADE)

    class Meta:
        db_table = 'xplaylist'

    def __str__(self):
        return self.name


class XplaylistVideo(models.Model):
    playlist = models.ForeignKey(XPlaylist, on_delete=models.CASCADE)
    video = models.ForeignKey(XVideo, on_delete=models.CASCADE)

    class Meta:
        db_table = 'xplaylist_video'

    def __str__(self):
        return self.playlist.name + ' | ' + self.video.name


class XReportTypes(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'xreport_types'


class XReport(models.Model):
    id = models.AutoField(primary_key=True)
    r_type = models.ForeignKey(XReportTypes, on_delete=models.CASCADE)
    r_text = models.CharField(max_length=254, blank=True, null=True)
    r_date = models.DateTimeField(auto_now_add=True, blank=True)
    r_video = models.ForeignKey(XVideo, on_delete=models.CASCADE)

    class Meta:
        db_table = 'xreport'


class XSearchHistory(models.Model):
    user = models.ForeignKey(XUser, on_delete=models.CASCADE)
    keyword = models.CharField(max_length=255, null=False)
    s_date = models.DateField(auto_now_add=True)

    class Meta:
        db_table = 'xsearch_history'


class XSubscribe(models.Model):
    from_u = models.ForeignKey(XUser, on_delete=models.CASCADE, related_name='from_id')
    to = models.ForeignKey(XUser, on_delete=models.CASCADE, related_name='to_id')

    class Meta:
        db_table = 'xsubscribe'

    def __str__(self):
        return str(self.from_u) + '->' + str(self.to)

class XTag(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        db_table = 'xtag'

    def __str__(self):
        return self.name


class XVideoTag(models.Model):
    video = models.ForeignKey(XVideo, on_delete=models.CASCADE)
    tag = models.ForeignKey(XTag, on_delete=models.CASCADE)

    class Meta:
        db_table = 'xvideo_tag'

    def __str__(self):
        return self.tag.name + '|' + self.video.name


class XWatchHistory(models.Model):
    user = models.ForeignKey(XUser, on_delete=models.CASCADE)
    video = models.ForeignKey(XVideo, on_delete=models.CASCADE)
    w_date = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        db_table = 'xwatch_history'
