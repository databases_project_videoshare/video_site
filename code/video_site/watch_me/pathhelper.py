import os


def set_file_path(instance, filename):
    return os.path.join(str(instance.uploader_id), filename)


def profile_path(instance, filename):
    return os.path.join(str(instance.id), 'profile.jpg')